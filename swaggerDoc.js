const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    // Like the one described here: https://swagger.io/specification/#infoObject
    info: {
      title: 'Baseline API',
      version: '1.0.0',
      description: '',
      tryItOut: false
    },
  },
  // List of files to be processes. You can also set globs './routes/*.js'
  apis: ['./routes/*.js'],
};

const DisableTryItOutPlugin = function() {
return {
    statePlugins: {
        spec: {
            wrapSelectors: {
            allowTryItOutFor: () => () => false
            }
        }
    }
}
}

const DisableAuthorizePlugin = function() {
  return {
    wrapComponents: {
      authorizeBtn: () => () => null
    }
  }
}

var options2 = {
    swaggerOptions: {
        plugins: [
            DisableTryItOutPlugin,
            DisableAuthorizePlugin
        ]
     }
  };

const specs = swaggerJsdoc(options);

module.exports = (app) => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs, options2));
}