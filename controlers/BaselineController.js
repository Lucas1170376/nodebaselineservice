let baselineService = require('../services/BaselineService')
let validator = require('../services/ValidationsService')
const { router } = require('../app')

exports.getAvailableMethods = function(router){
    router.get('/availableMethods', async(req, res) => {
        let availableMethods = baselineService.getAvailableMethods()
        return res.status(200).send({ "methods": availableMethods })
    })
}

exports.getSelectedBaselines = function(router){
    router.get('/selectedBaselines', async(req, res) => {
        let consumptionDataset = req.body.consumptionDataset;
        let adjustmentValues = req.body.adjustmentValues;
        let targetPeriodIndex = req.body.targetPeriodIndex;
        let methodsList = req.body.methodsList

        let trueOrErrorMessage = validator.validateConsumptionMatrix(consumptionDataset, targetPeriodIndex)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        trueOrErrorMessage = validator.validateAdjustmentParameters(adjustmentValues)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        let result = baselineService.getSelectedBaselines(consumptionDataset, adjustmentValues, targetPeriodIndex, methodsList);
        if (result == false) {
            return res.status(400).send({ "error": "something went wrong, probably with the input you sent to this service" })
        }
        return res.status(200).send({ "baselines": result })

    })
}

exports.getAllBaselines = function(router){
    router.get('/allBaselines', async(req, res) => {

        let consumptionDataset = req.body.consumptionDataset;
        let adjustmentValues = req.body.adjustmentValues;
        let targetPeriodIndex = req.body.targetPeriodIndex;

        let trueOrErrorMessage = validator.validateConsumptionMatrix(consumptionDataset, targetPeriodIndex)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        trueOrErrorMessage = validator.validateAdjustmentParameters(adjustmentValues)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        let result = baselineService.getAllBaselines(consumptionDataset, adjustmentValues, targetPeriodIndex);
        if (result == false) {
            return res.status(400).send({ "error": "something went wrong, probably with the input you sent to this service" })
        }
        return res.status(200).send({ "baselines": result })
    })
}

exports.getBaselineByMultiplicationOfAverageValues = function (router) {
    router.get('/getBaselineByMultiplicationOfAverageValues', async (req, res) => {
        let consumptionDataset = req.body.consumptionDataset;
        let adjustmentValues = req.body.adjustmentValues;
        let targetPeriodIndex = req.body.targetPeriodIndex;

        let trueOrErrorMessage = validator.validateConsumptionMatrix(consumptionDataset, targetPeriodIndex)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        trueOrErrorMessage = validator.validateAdjustmentParameters(adjustmentValues)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        let result = baselineService.getBaselineByMultiplicationOfAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex);
        if (result == false) {
            return res.status(400).send({ "error": "something went wrong, probably with the input you sent to this service" })
        }
        return res.status(200).send({ "baseline": result })
    });
}

exports.getBaselineBySubtractingAverageValues = function (router) {
    router.get('/getBaselineBySubtractionOfAverageValues', async (req, res) => {
        let consumptionDataset = req.body.consumptionDataset;
        let adjustmentValues = req.body.adjustmentValues;
        let targetPeriodIndex = req.body.targetPeriodIndex;

        let trueOrErrorMessage = validator.validateConsumptionMatrix(consumptionDataset, targetPeriodIndex)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        trueOrErrorMessage = validator.validateAdjustmentParameters(adjustmentValues)
        if (trueOrErrorMessage != true) {
            return res.status(400).send({ "error": trueOrErrorMessage })
        }

        let result = baselineService.getBaselineBySubtractingAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex)
        if (result == false) {
            return res.status(400).send({ "error": "something went wrong, probably with the input you sent to this service" })
        }
        return res.status(200).send({ "baseline": result })
    });
}