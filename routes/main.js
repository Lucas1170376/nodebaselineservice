//THIS FILE IS NOT BEING USED AT ALL

var express = require('express');
var swaggerJsdoc = require('swagger-jsdoc');
var swaggerUi = require('swagger-ui-express');
var router = express.Router();
var controller = require('../controlers/baselines');

router.get('/api/baseline/:adjustmentMethod', function (req, res) {
    let method = req.params.adjustmentMethod
    console.log("\nMethod is: " + method);
    let consumptionMatrix = req.body.consumptionMatrix
    let periodIndexesUsedForAdjustment = req.body.periodIndexesUsedForAdjustment
    //? why this &&
    let targetPeriodIndex = req.body.targetPeriodIndex && req.body.targetPeriodIndex
});

// api/baselines (BODY: lista de baselines a executar)

//todo change variable names in the below docs

/** 
 * @swagger
 * /mult/avg:
 *  get:
*     tags:
*       - Baseline
*     name: Login
*     summary: Calculates the baseline according to the last 10 days. The adjust considering past 2 hours is done using a multiplication
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           required:
*             - past_days
*             - past_hours
*           properties:
*             past_days:
*               type: array
*               items:
*                 type: array
*                 items:
*                   type: integer
*             past_hours:
*               type: array
*               items:
*                 type: integer
*             targetPeriodIndex:
*               type: integer
*     responses:
*       200:
*         description: Baseline calculated.
*       401:
*         description: Bad input data.
*/
router.get('/mult/avg', function (req, res, next) {
    var consumptionMatrix = req.body.consumptionMatrix
    var periodIndexesUsedForAdjustment = req.body.periodIndexesUsedForAdjustment
    //? why this &&
    var targetPeriodIndex = req.body.targetPeriodIndex && req.body.targetPeriodIndex

    if (!Array.isArray(consumptionMatrix) || !Array.isArray(periodIndexesUsedForAdjustment))
        return res.status(400).send({ "error": "must use arrays" })
    if (consumptionMatrix.length < 10 || periodIndexesUsedForAdjustment.length < 2)
        return res.status(400).send({ "error": "must use the right size" })

    //todo test if the returning of an exit status works with this
    var result = controller.multAvg(consumptionMatrix, periodIndexesUsedForAdjustment, targetPeriodIndex)
    return res.status(200).send({ "baseline": result })
});

/** 
 * @swagger
 * /dif/avg:
 *  get:
*     tags:
*       - Baseline
*     name: Login
*     summary: Calculates the baseline according to the last 10 days. The adjust considering past 2 hours is done using the middle point difference
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           required:
*             - past_days
*             - past_hours
*           properties:
*             past_days:
*               type: array
*               items:
*                 type: array
*                 items:
*                   type: integer
*             past_hours:
*               type: array
*               items:
*                 type: integer
*             targetPeriodIndex:
*               type: integer
*     responses:
*       200:
*         description: Baseline calculated.
*       401:
*         description: Bad input data.
*/
router.get('/dif/avg', function (req, res, next) {
    var consumptionMatrix = req.body.consumptionMatrix
    var periodIndexesUsedForAdjustment = req.body.periodIndexesUsedForAdjustment
    var targetPeriodIndex = req.body.targetPeriodIndex && req.body.targetPeriodIndex

    if (!Array.isArray(consumptionMatrix) || !Array.isArray(periodIndexesUsedForAdjustment))
        return res.status(400).send({ "error": "must use arrays" })
    if (consumptionMatrix.length < 10 || periodIndexesUsedForAdjustment.length < 2)
        return res.status(400).send({ "error": "must use the right size" })

    //todo also test the above here
    var result = controller.multAvg(consumptionMatrix, periodIndexesUsedForAdjustment, targetPeriodIndex)
    return res.status(200).send({ "baseline": result })
});

module.exports = router;
