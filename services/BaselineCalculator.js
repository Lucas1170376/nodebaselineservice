function getBaselineWithoutAdjustment(consumptionDataset, targetPeriodIndex){
    let averagesForEachPeriod = getArrayOfAveragesForEachPeriod(consumptionDataset)
    if(averagesForEachPeriod == false)
        return false

    let baseline = averagesForEachPeriod[targetPeriodIndex]
    if (baseline < 0)
        baseline = 0
    return baseline
}
exports.getBaselineWithoutAdjustment = getBaselineWithoutAdjustment


function getBaselineByMultiplicationOfAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex){
    var averagesForEachPeriod = getArrayOfAveragesForEachPeriod(consumptionDataset)
    if(averagesForEachPeriod == false)
        return false

    let sum = 0;
    for(let i = 0; i < adjustmentValues.length; i++){
        let periodIndex = adjustmentValues[i].periodIndex;
        if(periodIndex >= averagesForEachPeriod.length){
            return false
        }
        let realValue = adjustmentValues[i].realValue;
        sum += (realValue / averagesForEachPeriod[periodIndex]);
    }
    let adjustmentValue = sum / adjustmentValues.length;

    let baseline = averagesForEachPeriod[targetPeriodIndex] * adjustmentValue
    if (baseline < 0)
        baseline = 0
    return baseline
}
exports.getBaselineByMultiplicationOfAverageValues = getBaselineByMultiplicationOfAverageValues


function getBaselineBySubtractingAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex){
    var averagesForEachPeriod = getArrayOfAveragesForEachPeriod(consumptionDataset)
    if(averagesForEachPeriod == false)
        return false

    let sumFromDataset = 0;
    for(let i = 0; i < adjustmentValues.length; i++){
        let periodIndex = adjustmentValues[i].periodIndex;
        sumFromDataset += averagesForEachPeriod[periodIndex]
    }
    let meanFromDataset = sumFromDataset / adjustmentValues.length;

    let sumFromRealValues = 0;
    for(let i = 0; i < adjustmentValues.length; i++){
        sumFromRealValues += adjustmentValues[i].realValue;
    }
    let meanFromRealValues = sumFromRealValues / adjustmentValues.length

    let adjustmentValue = meanFromDataset - meanFromRealValues;
    let baseline = averagesForEachPeriod[targetPeriodIndex] - adjustmentValue;

    if (baseline < 0)
        baseline = 0
    return baseline
}
exports.getBaselineBySubtractingAverageValues = getBaselineBySubtractingAverageValues


function getArrayOfAveragesForEachPeriod(consumptionDataset) {
    numberOfDays = consumptionDataset.length
    numberOfPeriods = consumptionDataset[0].length

    averagesForEachPeriod = new Array(numberOfPeriods).fill(0);
    for (var i = 0; i < numberOfDays; i++) {
        if (consumptionDataset[i].length < numberOfPeriods)
            return false
        for (var j = 0; j < numberOfPeriods; j++)
            averagesForEachPeriod[j] += consumptionDataset[i][j]
    }

    for(let i = 0; i < numberOfPeriods; i++){
        averagesForEachPeriod[i] /= numberOfDays 
    }

    return averagesForEachPeriod
}