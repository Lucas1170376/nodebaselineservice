function validateConsumptionMatrix(consumptionDataset, targetPeriodIndex){

    if (!Array.isArray(consumptionDataset)) {
        return "the consumption dataset must be in array(matrix) format"
    }

    if (consumptionDataset[0].length < targetPeriodIndex) {
        return "number of columns in the consumption dataset is lower than the target period index ";
    }

    if (consumptionDataset.length < 2) {
        return "the consumption data has to be at least 2 days long, in order to use at least 2 previous days"
    }

    return true

}
exports.validateConsumptionMatrix = validateConsumptionMatrix


function validateAdjustmentParameters(adjustmentValues){

    if (!Array.isArray(adjustmentValues)) {
        return "the adjustment values must be an array of objects of type {periodIndex=x,value=y}"
    }

    if (adjustmentValues.length < 1) {
        return "the adjustment values can not be empty"
    }
    
    return true

}
exports.validateAdjustmentParameters = validateAdjustmentParameters


function validateBaselinesList(baselines){

    if (!Array.isArray(baselines)) {
        return "the baseline parameter must be an array of baseline data objects"
    }

    if (baselines.length < 1) {
        return "the baseline array cannot be empty"
    }

    return true

}
exports.validateBaselinesList = validateBaselinesList


//? if another method for computing the baseline is to be added, add a variable and add it to the array below
//? and also add it to the switch case statement below and the one on the BaselineService.js
let baselineByMultiplicationOfAverageValuesString = "multiplication_of_average_values"
let baselineBySubtractionOfAverageValuesString = "subtraction_of_average_values"
let noAdjustmentString = "no_adjustment"
let methodsArray = [
    baselineByMultiplicationOfAverageValuesString, 
    baselineBySubtractionOfAverageValuesString, 
    noAdjustmentString
]

function validateAdjustmentMethod(method){
    switch(method){
        case baselineByMultiplicationOfAverageValuesString:
            return true
        case baselineBySubtractionOfAverageValuesString:
            return true
        case noAdjustmentString:
            return true
        default:
            return methodsArray    
    }
}
exports.validateAdjustmentMethod = validateAdjustmentMethod