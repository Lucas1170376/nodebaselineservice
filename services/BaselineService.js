let baselineCalculator = require('./BaselineCalculator')
let validator = require('./ValidationsService')

//todo test all validations
//? if another method for computing the baseline is to be added, add a variable and add it to the array below
//? and also add it to the validator
let baselineByMultiplicationOfAverageValuesString = "multiplication_of_average_values"
let baselineBySubtractionOfAverageValuesString = "subtraction_of_average_values"
let noAdjustment = "no_adjustment"
let methodsArray = [
    baselineByMultiplicationOfAverageValuesString,
    baselineBySubtractionOfAverageValuesString,
    noAdjustment
]

function getAvailableMethods() {
    return methodsArray
}
exports.getAvailableMethods = getAvailableMethods

function getSelectedBaselines(consumptionDataset, adjustmentValues, targetPeriodIndex, methodsList) {
    let datasetSize = consumptionDataset.length;
    let previousDays = datasetSize
    let response = []

    // first we send the whole dataset
    methodsArray.forEach(method => {
        if (methodsList.includes(method)) {
            var baseline = getBaselineGivenMethod(consumptionDataset, adjustmentValues, targetPeriodIndex, method)
            if (baseline == false) {
                return false;
            }
            response.push({ "previousDays": previousDays, "method": method, "baseline": baseline })
        }
    })

    //then start sending pieces of the dataset, like just the X - 1 previous days, X - 2 previous days and so on
    // here I use datasetSize - 2 the last 2 days at the minimum
    let newConsumptionDataset = consumptionDataset
    for (let i = 0; i < datasetSize - 2; i++) {
        newConsumptionDataset.splice(0, 1)
        previousDays -= 1
        methodsArray.forEach(method => {
            if (methodsList.includes(method)) {
                var baseline = getBaselineGivenMethod(consumptionDataset, adjustmentValues, targetPeriodIndex, method)
                if (baseline == false) {
                    return false;
                }
                response.push({ "previousDays": previousDays, "method": method, "baseline": baseline })
            }
        })
    }

    return response

}
exports.getSelectedBaselines = getSelectedBaselines

function getAllBaselines(consumptionDataset, adjustmentValues, targetPeriodIndex) {
    let datasetSize = consumptionDataset.length;
    let previousDays = datasetSize
    let response = []

    // first we send the whole dataset
    methodsArray.forEach(method => {
        var baseline = getBaselineGivenMethod(consumptionDataset, adjustmentValues, targetPeriodIndex, method)
        if (baseline == false) {
            return false;
        }
        response.push({ "previousDays": previousDays, "method": method, "baseline": baseline })
    })

    //then start sending pieces of the dataset, like just the X - 1 previous days, X - 2 previous days and so on
    // here I use datasetSize - 2 the last 2 days at the minimum
    let newConsumptionDataset = consumptionDataset
    for (let i = 0; i < datasetSize - 2; i++) {
        newConsumptionDataset.splice(0, 1)
        previousDays -= 1
        methodsArray.forEach(method => {
            var baseline = getBaselineGivenMethod(consumptionDataset, adjustmentValues, targetPeriodIndex, method)
            if (baseline == false) {
                return false;
            }
            response.push({ "previousDays": previousDays, "method": method, "baseline": baseline })
        })
    }

    return response
}
exports.getAllBaselines = getAllBaselines

function getBaselineGivenMethod(consumptionDataset, adjustmentValues, targetPeriodIndex, adjustmentMethod) {
    switch (adjustmentMethod) {
        case baselineByMultiplicationOfAverageValuesString:
            return getBaselineByMultiplicationOfAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex)
        case baselineBySubtractionOfAverageValuesString:
            return getBaselineBySubtractingAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex)
        case noAdjustment:
            return getBaselineWithoutAdjustment(consumptionDataset, targetPeriodIndex)
        default:
            //in principle this would never happen because we validate everything when the controller calls the validator functions
            return false
    }
}
exports.getBaselineGivenMethod = getBaselineGivenMethod


function getBaselineWithoutAdjustment(consumptionDataset, targetPeriodIndex) {
    return baselineCalculator.getBaselineWithoutAdjustment(consumptionDataset, targetPeriodIndex)
}


function getBaselineByMultiplicationOfAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex) {
    return baselineCalculator.getBaselineByMultiplicationOfAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex)
}
exports.getBaselineByMultiplicationOfAverageValues = getBaselineByMultiplicationOfAverageValues


function getBaselineBySubtractingAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex) {
    return baselineCalculator.getBaselineBySubtractingAverageValues(consumptionDataset, adjustmentValues, targetPeriodIndex)
}
exports.getBaselineBySubtractingAverageValues = getBaselineBySubtractingAverageValues